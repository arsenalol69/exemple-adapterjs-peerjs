import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';

Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  var Peer = require('peerjs');
  var Adapter = require('adapterjs');
  
  window.peer = new Peer({
		  key: "test",  // get a free key at http://peerjs.com/peerserver
		  debug: 3,
		  config: {'iceServers': [
		    { url: 'stun:stun.l.google.com:19302' },
		    { url: 'stun:stun1.l.google.com:19302' },
		  ]}
	    });
		
  this.counter = new ReactiveVar(0);
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});
